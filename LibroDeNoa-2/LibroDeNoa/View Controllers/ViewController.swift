//
//  ViewController.swift
//  LibroDeNoa
//
//  Created by Enric Vergara Carreras on 5/12/17.
//  Copyright © 2017 dedam. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ViewController: UIViewController {
    
    var bombSoundEffect: AVAudioPlayer?
    var timer = Timer()
    var timeCounter:Int = 3
    var timeCounter1:Int = 3
    var timeCounter2:Int = 3
    var timeCounter3:Int = 3

    var troncClicked = false
    var tietclicked = false
    var noaclicked = false
    var menuclicked = false
    var tiaclicked = false
    var gatoclicked = false
    var regadoraClicked = false
    var ocellclicked = false
    var sargantanaclicked = false
   
    
    
    //---IBOutlets:
    @IBOutlet weak var btn_menu: UIButton!
    @IBOutlet weak var menu: UIView!
    @IBOutlet weak var img_tietCos: UIImageView!
    @IBOutlet weak var Noa_entera: UIView!
    @IBOutlet weak var btn_cosnoa: UIButton!
    @IBOutlet weak var Noa_Cos: UIImageView!
    @IBOutlet weak var Noa_Cap: UIImageView!
    @IBOutlet weak var Ocell: UIImageView!
    @IBOutlet weak var Tia_Cap: UIImageView!
    @IBOutlet weak var Tia_Cos: UIImageView!
    @IBOutlet weak var btn_regadora: UIButton!
    @IBOutlet weak var btn_text_gato: UIButton!
    @IBOutlet weak var text_tia: UIView!
    @IBOutlet weak var btn_text_tia: UIButton!
    @IBOutlet weak var lbl_narrador: UILabel!
    @IBOutlet weak var lbl_gato: UILabel!
    @IBOutlet weak var lbl_tia: UILabel!
    @IBOutlet weak var text_noa: UIView!
    @IBOutlet weak var text_tiet: UIView!
    @IBOutlet weak var lbl_noa: UILabel!
    @IBOutlet weak var lbl_tiet: UILabel!
    @IBOutlet weak var btn_text_tiet: UIButton!
    @IBOutlet weak var btn_text_noa: UIButton!
  
    @IBOutlet weak var text_gato: UIView!
    @IBOutlet weak var btn_ocell: UIButton!
    
    @IBOutlet weak var btn_sargantana: UIButton!
    @IBOutlet weak var Sargantana_Cua: UIImageView!
    @IBOutlet weak var Sargantana_Cos: UIImageView!
    //------------
    
    //---IBActions:
    @IBAction func btn_menu_pressed(_ sender: Any) {
        
        if(!menuclicked){
            menu.isHidden = false
            
        }
    }
    
    @IBAction func btn_sargantana_pressed(_ sender: Any) {
        if(!sargantanaclicked) {
            Sargantana_Cos.stopAnimating()
            
            config_SargantanaCua()
            Sargantana_Cos.startAnimating()
            Sargantana_Cua.isHidden = false
            
            
            //TODO: play sound "copfusta_tiet2.mp3"
        }
        
    }
    @IBAction func btn_text_gato_pressed(_ sender: Any) {
        
        if(!gatoclicked){
            text_gato.isHidden = false
            lbl_gato.text = NSLocalizedString("key_gato", comment: "")
            timeCounter3 = 3
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeup), userInfo: nil, repeats: true)
        }
    }
    @IBAction func btn_noa_pressed(_ sender: Any) {
        
        if(!noaclicked){
            text_noa.isHidden = false
            lbl_noa.text = NSLocalizedString("key_niña", comment: "")
            timeCounter2 = 3
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeup), userInfo: nil, repeats: true)
        }
        
    }
    @IBAction func btn_text_tiet_pressed(_ sender: Any) {
        
        if(!tietclicked){
            text_tiet.isHidden = false
            lbl_tiet.text = NSLocalizedString("key", comment: "")
            timeCounter = 3
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeup), userInfo: nil, repeats: true)
            
            
        }
    }
    @IBAction func btn_text_tia_pressed(_ sender: Any) {
       
        if(!tiaclicked){
            text_tia.isHidden = false
            lbl_tia.text = NSLocalizedString("key_tia", comment: "")
            timeCounter1 = 3
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeup), userInfo: nil, repeats: true)
            
            
        
        }
    }
   
    @IBAction func tia_Cos_Pressed(_ sender: Any) {
        if(!regadoraClicked) {
            Tia_Cos.stopAnimating()
            config_TiaRegant()
            Tia_Cos.startAnimating()
            
            
       }
    }
    
    @IBAction func tietCos_Pressed(_ sender: Any) {
        if(!troncClicked) {
            img_tietCos.stopAnimating()
            config_TietTronc()
            img_tietCos.startAnimating()
            
            //TODO: play sound "copfusta_tiet2.mp3"
        }
    }
   
    @IBAction func btn_ocell_Pressed(_ sender: Any) {
        if(!ocellclicked) {
            Ocell.stopAnimating()
           config_Ocell()
            Ocell.startAnimating()

    }
        
    }
    @objc func timeup(){
        if timeCounter1 < 1 {
            timer.invalidate()
            text_tia.isHidden = true
            
        }else{
            timeCounter1 -= 1
            print(timeCounter)
        }
        
        if timeCounter2 < 1 {
            timer.invalidate()
            text_noa.isHidden = true
            
        }else{
            timeCounter2 -= 1
            print(timeCounter)
        }
        
        if timeCounter3 < 1 {
            timer.invalidate()
            text_gato.isHidden = true
            
        }else{
            timeCounter3 -= 1
            print(timeCounter)
        }
        
        if timeCounter < 1 {
            timer.invalidate()
            text_tiet.isHidden = true
            
        }else{
         timeCounter -= 1
        print(timeCounter)
    }
    }
    //-------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        lbl_narrador.text = NSLocalizedString("key_narrador", comment: "")
        
        config_SargantanaNormal()
        Sargantana_Cos.startAnimating()
        
        config_TietNormal()
        img_tietCos.startAnimating()
        
        config_TiaNormal()
        Tia_Cos.startAnimating()
        
        config_Noa()
        Noa_Cos.startAnimating()
        
        playSound(fileName:NSLocalizedString("sonidonarrador", comment: ""), _extension: "mp3")
        
        text_gato.isHidden = true
        text_noa.isHidden = true
        text_tiet.isHidden = true
        text_tia.isHidden = true
        
        Sargantana_Cua.isHidden = true
        menu.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func playSound(fileName:String, _extension:String){
        let path = Bundle.main.path(forResource: fileName, ofType:_extension)!
        let url = URL(fileURLWithPath: path)
        
       do {
            bombSoundEffect = try AVAudioPlayer(contentsOf: url)
            bombSoundEffect?.play()
       } catch {
        
          print("couldn't load file :(")
        }
    }
    private func config_SargantanaNormal(){
        sargantanaclicked = false;
        
        let sargantana1 = UIImage(named: "01_Sargantana1.png")
        let sargantana2 = UIImage(named: "01_Sargantana2.png")
        Sargantana_Cos.animationImages = [sargantana1!, sargantana2!]
        Sargantana_Cos.animationRepeatCount = 0
        Sargantana_Cos.animationDuration = 2.0
    }
    private func config_SargantanaCua(){
        sargantanaclicked = true;
        
        let sargantana1 = UIImage(named: "01_Sargantana_escuada1.png")
        let sargantana2 = UIImage(named: "01_Sargantana_escuada2.png")
    
        Sargantana_Cos.animationImages = [sargantana1!, sargantana2!]
        Sargantana_Cos.animationRepeatCount = 0
        Sargantana_Cos.animationDuration = 4.0
        
        UIView.animate(withDuration: 4, animations:{
            self.Sargantana_Cos.frame.origin.y = self.view.frame.size.height - 850
            self.Sargantana_Cua.frame.origin.y = self.view.frame.size.height - 300
            self.Sargantana_Cos.alpha = 1
            self.Sargantana_Cua.alpha = 0.25
        },completion: nil)
    }
    
    private func config_Noa(){
        
        
        let noacos1 = UIImage(named: "01_Carlota_bici01.png")
        let noacos2 = UIImage(named: "01_Carlota_bici04.png")
        let noacos3 = UIImage(named: "01_Carlota_bici02.png")
        let noacos4 = UIImage(named: "01_Carlota_bici05.png")
        let noacos5 = UIImage(named: "01_Carlota_bici03.png")
        let noacos6 = UIImage(named: "01_Carlota_bici06.png")
        let noacos7 = UIImage(named: "01_Carlota_bici08.png")
        let noacos8 = UIImage(named: "01_Carlota_bici07.png")
        let noacos9 = UIImage(named: "01_Carlota_bici09.png")
        let noacos10 = UIImage(named: "01_Carlota_bici11.png")
        let noacos11 = UIImage(named: "01_Carlota_bici10.png")
        let noacos12 = UIImage(named: "01_Carlota_bici12.png")
        Noa_Cos.animationImages = [noacos1!, noacos2!, noacos3!, noacos4!, noacos5!, noacos6!, noacos7!, noacos8!, noacos9!, noacos10!, noacos11!, noacos12!]
        Noa_Cos.animationRepeatCount = 2
        Noa_Cos.animationDuration = 2.0
        
        Noa_entera.frame.origin.x = -Noa_entera.frame.size.width
        Noa_entera.frame.origin.y = 191
        
        UIView.animate(withDuration: 4, animations:{
            self.Noa_entera.frame.origin.x = self.view.frame.size.width - 600
            self.Noa_entera.alpha = 1},completion: nil)
        
    }
    
    private func config_Ocell(){
        ocellclicked = true;
        
        let ocell1 = UIImage(named: "01_ocell_empren_vol.png")
        let ocell2 = UIImage(named: "01_ocell_vola1.png")
        let ocell3 = UIImage(named: "01_ocell_vola2.png")
        Ocell.animationImages = [ocell1!, ocell2!, ocell3!]
        Ocell.animationRepeatCount = 0
        Ocell.animationDuration = 2.5
        
        
        UIView.animate(withDuration: 8, animations:{self.Ocell.center.x += self.view.bounds.width
            self.Ocell.alpha = 1},completion: nil)
        
    }
    private func config_TietNormal(){
        troncClicked = false;
        
        let tiet1 = UIImage(named: "01_tiet_copet_destral1.png")
        let tiet2 = UIImage(named: "01_tiet_estatic.png")
        img_tietCos.animationImages = [tiet1!, tiet2!]
        img_tietCos.animationRepeatCount = 0
        img_tietCos.animationDuration = 2.0
    }
    
    private func config_TietTronc(){
        troncClicked = true;
        
        let tiet1 = UIImage(named: "01_tiet_Partint_tronc1.png")
        let tiet2 = UIImage(named: "01_tiet_Partint_tronc2.png")
        let tiet3 = UIImage(named: "01_tiet_Partint_tronc3.png")
        img_tietCos.animationImages = [tiet1!, tiet2!, tiet3!]
        img_tietCos.animationRepeatCount = 1
        img_tietCos.animationDuration = 1.5
        
    }
    
    private func config_TiaNormal(){
        regadoraClicked = false;
        
        let tia1 = UIImage(named: "01_tieta_Regant_01.png")
        let tia2 = UIImage(named: "01_tieta_estatica.png")
        Tia_Cos.animationImages = [tia1!, tia2!]
        Tia_Cos.animationRepeatCount = 0
        Tia_Cos.animationDuration = 2.0
    }
    
    private func config_TiaRegant(){
        troncClicked = true;
        
        let tia1 = UIImage(named: "01_tieta_Regant_01.png")
        let tia2 = UIImage(named: "01_tieta_Regant_02.png")
        let tia3 = UIImage(named: "01_tieta_estatica.png")
        Tia_Cos.animationImages = [tia1!, tia2!, tia3!]
        Tia_Cos.animationRepeatCount = 1
        Tia_Cos.animationDuration = 2.5
        
    }


}

